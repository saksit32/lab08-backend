package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("MyDao")
@Repository
public class MyStudentDao implements StudentDao {
    List<Student> students;
    public MyStudentDao() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("5921150029")
                .name("nian")
                .surname("NandN")
                .gpa(4.30)
                .image("http://34.218.248.103:8887/images/one.jpg")
                .penAmount(120)
                .description("OO YES")
                .build());

    }


    @Override
    public List<Student> getStudents() {
        log.info("My Dao is called.....");
        return this.students;
    }

    @Override
    public Student getStudent(Long id) {
        return this.students.get(Math.toIntExact(id-1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) this.students.size());
        this.students.add(student);
        return student;
    }
}
